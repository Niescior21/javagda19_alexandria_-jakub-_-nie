package Customers;

import Customers.Customer;

import java.util.ArrayList;

public class CustomerAp {
    private String customerFullname;
    private ArrayList<Customer> customers;


    public CustomerAp(String customerFullname) {
        this.customerFullname = customerFullname;
        this.customers = new ArrayList<Customer>();
    }


    public boolean addNewCustomer(Customer customer) {
        if (findCustomer(customer.getCustomerFullname()) >= 0) {
            System.out.println("customer is already on file");
            return false;
        }
        customers.add(customer);
        return true;
    }

    private int findCustomer(Customer customer) {
        return customers.indexOf(customer);
    }

    private int findCustomer(String customerFullname) {
        for (int i = 0; i < customers.size(); i++) {
            Customer customer = customers.get(i);
            if (customer.getCustomerFullname().equals(customerFullname)) {
                return i;
            }
        }
        return -1;

    }

    public boolean removeCustomer(Customer customer) {
        int foundPosition = findCustomer(customer);
        if (foundPosition < 0) {
            System.out.println(customer.getCustomerFullname() + " was not found");
            return false;
        }
        customers.remove(foundPosition);
        System.out.println(customer.getCustomerFullname() + "was removed");
        return true;


    }

    public String queryCustomer(Customer customer) {
        if (findCustomer(customer) >= 0) {
            return customer.getCustomerFullname();
        }
        return null;
    }

    public Customer queryCustomer(String name) {
        int position = findCustomer(name);
        if (position >= 0) {
            return customers.get(position);
        }
        return null;
    }


        public void printCustomers () {
            System.out.println("Customers.Customer list");
            for (int i = 0; i < customers.size(); i++) {
                System.out.println((i + 1) + "." +
                        customers.get(i).getCustomerFullname() + "->" +
                        customers.get(i).getCustomerEmail() + "->" +
                        customers.get(i).getCustomerPhoneNumber());


            }
        }

    }

