package Items;

public class Book extends Item {
    private int pageNumber;
    private int publicationDate;
    BookTypes bookTypes;

    public Book(String title, String author, int pageNumber, int publicationDate, BookTypes bookTypes) {
        super(title, author);
        this.pageNumber = pageNumber;
        this.publicationDate = publicationDate;
        this.bookTypes = bookTypes;
    }


    public BookTypes getBookTypes() {
        return bookTypes;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPublicationDate() {
        return publicationDate;
    }


    public static Book createNewBook(String title, String author, int pageNumber, int publicationDate, BookTypes bookTypes) {
        return new Book(title, author,  pageNumber, publicationDate, bookTypes);
    }
}
