import Customers.Customer;
import Customers.CustomerAp;
import Items.Book;
import Items.BookAp;
import Items.BookTypes;
import Items.Item;

import java.util.Scanner;

public class AlexandriaLibrary {
     private static Scanner scanner = new Scanner(System.in);
     private static CustomerAp nieWiemCoRobie = new CustomerAp("Tim");
     private static BookAp niewiemCoRobie2 = new BookAp("titanic");
    public static void main(String[] args) {
        boolean quit = false;
        printActions();
        while (!quit) {
            System.out.println("\n Enter action:  ( 7 to show avaiable actions)");
            int action = scanner.nextInt();
            scanner.nextLine();




                    switch  (action) {
                        case 0:
                            quit = true;
                            break;
                        case 1:
                            nieWiemCoRobie.printCustomers();
                            break;
                        case 2:
                            addCustomer();
                            break;
                        case 3:
                            removeCustomer();
                            break;
                        case 4:
                            queryCustomer();
                            break;
                        case 5:
                            niewiemCoRobie2.printBooks();
                            break;
                        case 6:
                            addBook();
                            break;

                        case 7:
                            printActions();
                            break;



                    }

            }
        }

           public static void addCustomer(){
               System.out.println("Enter Customers.Customer Fullname");
               String fullname = scanner.nextLine();
               System.out.println("Enter Customers.Customer EmailAdress");
               String emailAdress = scanner.nextLine();
               System.out.println("Enter phone number");
               String phoneNumber = scanner.nextLine();
               Customer newCustomer = Customer.createNewCustomer(fullname,emailAdress,phoneNumber);
               if(nieWiemCoRobie.addNewCustomer(newCustomer)){
                   System.out.println("New customer added name : " + fullname + " email adress: " + emailAdress+ " phone number: "+ phoneNumber);

               } else {
                   System.out.println("Cannot add, " + fullname + "already on file");
               }

            }
            public static void removeCustomer() {
                System.out.println("Enter Fullname of customer you want to remove");
                String fullname = scanner.nextLine();
                Customer existingCustomer = nieWiemCoRobie.queryCustomer(fullname);
                if (existingCustomer == null) {
                    System.out.println("Contact not found");
                    return;
                }
                if (nieWiemCoRobie.removeCustomer(existingCustomer)) {
                    System.out.println("Succesfully deleted");
                } else {
                    System.out.println("Error deleting customer");


                }
            }
            public static void queryCustomer() {
                System.out.println("Enter existing Customers.Customer name");
                String name = scanner.nextLine();
                Customer alreadyExistingCustomer = nieWiemCoRobie.queryCustomer(name);
                if (alreadyExistingCustomer == null) {
                    System.out.println("Customers.Customer not found");
                    return;

                }
                System.out.println("Name "+ alreadyExistingCustomer.getCustomerFullname() + " email " + alreadyExistingCustomer.getCustomerEmail() + " phone number"+ alreadyExistingCustomer.getCustomerPhoneNumber());
            }


            public static void printActions () {
                System.out.println("\nAvailable actions : \npress");
                System.out.println("0 to quit\n" +
                        "1  to print Customers\n" +
                        "2 to add a new Customers.Customer\n" +
                        "3 to remove a customer\n" +
                        "4 to query a customer\n" +
                        "5 to print book list\n" +
                        "6 to add a book\n" +
                        "7 to show avaiable actions\n");
                System.out.println("Chose your action");


            }
            private static void addBook(){
                System.out.println("Enter a book title");
                String bookTitle = scanner.nextLine();
                System.out.println("Enter an author neme");
                String authorname = scanner.nextLine();
                System.out.println("Enter a page number");
                int pageNumber = scanner.nextInt();
                System.out.println("Enter a publication date");
                int publicationDate = scanner.nextInt();
                System.out.println("Enter a Genre type");
                BookTypes booktype = BookTypes.valueOf(scanner.nextLine());
                Book newBook = Book.createNewBook(bookTitle,authorname,pageNumber,publicationDate,booktype);
                if (niewiemCoRobie2.addNewBook(newBook)){
                    System.out.println("New book + " + bookTitle+ "Added");
                }else{
                    System.out.println("Cannot add this book already in file");
                }




            }





    }
