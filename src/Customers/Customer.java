package Customers;

public class Customer {
    private String customerFullname;
    private String customerEmail;
    private String customerPhoneNumber;

    public Customer(String customerFullname, String customerEmail, String customerPhoneNumber) {
        this.customerFullname = customerFullname;
        this.customerEmail = customerEmail;
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerFullname() {
        return customerFullname;
    }



    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public static Customer createNewCustomer(String customerFullname, String customerEmail, String customerPhoneNumber){
        return new Customer(customerFullname,customerEmail,customerPhoneNumber);
    }
}


