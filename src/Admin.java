public class Admin {
    private String adminName;
    private String adminSurname;
    private String adminEmail;
    private String adminPhoneNumber;

    public Admin(String adminName, String adminSurname, String adminEmail, String adminPhoneNumber) {
        this.adminName = adminName;
        this.adminSurname = adminSurname;
        this.adminEmail = adminEmail;
        this.adminPhoneNumber = adminPhoneNumber;
    }

    public String getAdminName() {
        return adminName;
    }

    public String getAdminSurname() {
        return adminSurname;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public String getAdminPhoneNumber() {
        return adminPhoneNumber;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminName='" + adminName + '\'' +
                ", adminSurname='" + adminSurname + '\'' +
                ", adminEmail='" + adminEmail + '\'' +
                ", adminPhoneNumber='" + adminPhoneNumber + '\'' +
                '}';
    }
}

