package Items;

import java.util.ArrayList;

public class BookAp {
    private String FullTitle;
    private ArrayList<Book> books;

    public BookAp(String fullTitle) {
        FullTitle = fullTitle;
        this.books = new ArrayList<Book>();

    }
    public boolean addNewBook(Book book) {
        if (findBook(book.getTitle()) >= 0) {
            System.out.println("book is already on file");
            return false;
        }
        books.add(book);
        return true;
    }


    private int findBook(Book book) {
        return books.indexOf(book);
    }

    private int findBook(String title) {
        for (int i = 0; i < books.size(); i++) {
            Book book = books.get(i);
            if (book.getTitle().equals(title)) {
                return i;
            }
        }
        return -1;

    }
    public boolean removeBook(Book book) {
        int foundPosition = findBook(book);
        if (foundPosition < 0) {
            System.out.println(book.getTitle() + " was not found");
            return false;
        }
        books.remove(foundPosition);
        System.out.println(book.getTitle() + "was removed");
        return true;
    }








        public void printBooks() {
        System.out.println("Books.book list");
        for (int i = 0; i < books.size(); i++) {
            System.out.println((i + 1) + "." +
                    books.get(i).getAuthor() + "->" +
                    books.get(i).getPublicationDate() + "->" +
                    books.get(i).getBookTypes() + "->" +
                    books.get(i).getPublicationDate() + "->" +
                    books.get(i).getPageNumber());
        }

    }

}