package Items;

public class Vinyl extends Item {
    MusicTypes musicTypes;

    public MusicTypes getMusicTypes() {
        return musicTypes;
    }

    public Vinyl(String title, String author, MusicTypes musicTypes) {
        super(title, author);
        this.musicTypes = musicTypes;

    }
}
